# russian-nesting-bathhouse

![challenge description](images/description.png)

Let's check out that zip file:

```
$ file russian-nesting-bathhouse.zip
russian-nesting-bathhouse.zip: gzip compressed data, from Unix, original size modulo 2^32 4014080
$ unzip russian-nesting-bathhouse.zip
Archive:  russian-nesting-bathhouse.zip
  End-of-central-directory signature not found.  Either this file is not
  a zipfile, or it constitutes one disk of a multi-part archive.  In the
  latter case the central directory and zipfile comment will be found on
  the last disk(s) of this archive.
unzip:  cannot find zipfile directory in one of russian-nesting-bathhouse.zip or
        russian-nesting-bathhouse.zip.zip, and cannot find russian-nesting-bathhouse.zip.ZIP, period.
```

Well that didn't work. I tried opening the zip file using the default gui application "File Roller", and it worked just fine, so I used that instead. After unzipping the files, we get the following:

```
$ ls
problem            russian-nesting-bathhouse.zip
$ cd problem
$ ls
bathhouse.zip      bathhouse_password
$ file bathhouse.zip
bathhouse.zip: Zip archive data, at least v2.0 to extract
$ file bathhouse_password
bathhouse_password: data
$ unzip bathhouse.zip
Archive:  bathhouse.zip
[bathhouse.zip] bathhouse/polish_cow.mp3 password:
```

So it looks like we need to retrieve the password from the bathhouse_password file. I tried running `strings`, but everything was garbled. However, we know from the hint that one of the files is a pdf that has been XOR'ed with an unknown 4 byte XOR key. This seems like a good candidate! 

XOR is reversible, which means that we can retrieve the key easily if we know what the original bytes are supposed to be and what the bytes are after the XOR. We know this file is supposed to be a pdf, so let's see if we can figure out what the first 4 bytes of a pdf file are supposed to be:

![google result](images/google1.png)

Cool! let's try to recover the key real quick:

```
$ xxd bathhouse_password | head -n 1
00000000: d6df 472f debe 2d5c f9aa c0cd 3033 c0df  ..G/..-\....03..
$ python -c "print(hex(0xd6df472f ^ 0x25504446))"
0xf38f0369
```

Now that we have our XOR key, we can (hopefully) just use [CyberChef](https://gchq.github.io/CyberChef/) to decrypt the file! Let's give it a shot:

![CyberChef result](images/cyberchef.png)

Looks like a pdf!! Let's open it:

![PDF with password](images/password.png)

Awesome! We can use that file to extract the next zip file, which gives us this:

```
$ unzip bathhouse.zip
Archive:  bathhouse.zip
[bathhouse.zip] bathhouse/polish_cow.mp3 password:
  inflating: bathhouse/polish_cow.mp3
$ cd bathhouse
$ ls
polish_cow.mp3
$ file polish_cow.mp3
polish_cow.mp3: Audio file with ID3 version 2.4.0, contains:MPEG ADTS, layer III, v1,  64 kbps, 48 kHz, Stereo
```

Ooooh an mp3 file. It's a song, but none of the words are in English. I used the "get info" utility in finder to see if anything interesting showed up:

![mp3 info](images/mp3info.png)

Wow, looks like someone hid a password in the "composer" tag! That might come in handy later on. That image looks a little suspiscious though, let's see if we can extract it.

```
$ binwalk --dd='.*' polish_cow.mp3

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
140           0x8C            JPEG image data, JFIF standard 1.01
2720546       0x298322        PARity archive data - file number 536

$ ls
polish_cow.mp3  _polish_cow.mp3.extracted
$ cd _polish_cow.mp3.extracted
$ ls
298322  8C
$ open 8C
```

![mp3 image](images/8C.jpg)

Wow, all sorts of stuff. Looks like we need to mess with the contrast and stuff a bit. Maybe that stegsolve program that Lyell mentioned in the description might be useful, let's try that. After loading the image into the program, we can step through the different filters with the arrows at the bottom. The "full red", "full green", and "full blue" made the different parts of the flag pop out, and I wrote them down: `osu{first_part_of_this_flag`

That doesn't look like the whole flag though. Maybe we should try steghide now, because why not? (Also because Ryan said we should and he was our coach lol)

```
steghide extract -sf 8C -xf extracted
Enter passphrase:
```

Hmmmm, it wants a password... Oh wait we have a password! Let's try that password that we found in the composer tag of the mp3 file!

```
Enter passphrase:
wrote extracted data to "extracted".
$ file extracted
extracted: gzip compressed data, from Unix
$ tar -xvf extracted
flag_part_2/flag_part_2.txt
$ cd flag_part_2
$ cat flag_part_2.txt
_dont_forget_5736h1d3}
```

Nice! So the complete flag is `osu{first_part_of_this_flag_dont_forget_5736h1d3}`