#nonsense

For this challenge, we are given a python file and a hostname/port that we can connect to. Let's try running the python program:

```
$ python3 server.py
Encrypted flag: bf18fe98e178aba3c11b038892332f4ab53771718a4e4bc8e01d956188b542814001a88349c91e899abebacdf13bd1c2b450ab7bde48c5d1b46e871dd57497
Please run a chosen plaintext attack
What message would you like to encrypt? aaaaaaaaaaaaaaaa
Nonce? aaaaaaaa
Traceback (most recent call last):
  File "server.py", line 25, in <module>
    print("Your message, encrypted: " + encrypt(nonce, message).hex())
  File "server.py", line 14, in encrypt
    cipher = AES.new(key, AES.MODE_OFB, iv=nonce)
  File "/home/allen/pyenv/lib/python3.6/site-packages/Crypto/Cipher/AES.py", line 232, in new
    return _create_cipher(sys.modules[__name__], key, mode, *args, **kwargs)
  File "/home/allen/pyenv/lib/python3.6/site-packages/Crypto/Cipher/__init__.py", line 79, in _create_cipher
    return modes[mode](factory, **kwargs)
  File "/home/allen/pyenv/lib/python3.6/site-packages/Crypto/Cipher/_mode_ofb.py", line 277, in _create_ofb_cipher
    factory.block_size)
ValueError: Incorrect IV length (it must be 16 bytes long)
```

Well that didn't work. Let's see what's happening in the code. Looking at [server.py](server.py), we can see that the `encrypt` function uses the AES algorithm in output feedback mode. The first thing that is printed is the initialization vector (`nonce`) followed by the ciphertext (encrypted flag) in hexadecimal. 

After that, the program tells us to "Please run a chosen plaintext attack" and allows us to encrypt something using the same algorithm and key as was used to encrypt the flag.

After a while of staring at the server code and getting a solve script template set up in pwntools, I decided I should probably figure out what OFB is. The Wikipedia article link that is in the [server.py](server.py) code has two images which describe the algorithm pretty well:

![OFB](images/img.png)

As you can probably tell, the encryption process is almost exactly the same as the decryption process, with the only exception being that the plaintext is XORed with the output for each block to obtain the ciphertext when encrypting, whereas the ciphertext is XORed with the output of each block to obtain the plaintext when decrypting. This essentially means that the exact same algorithm is used to decrypt the ciphertext as is used to encrypt it.

But wait! The program we have lets us encrypt any message using the exact same encryption algorithm and key as it used to encrypt the flag! So we have everything we need to be able to decrypt the flag!

The first 16 bytes that the program prints after "Encrypted flag: " are the initialization vector, and everything else is the ciphertext. To obtain the plaintext, we use the same initialization vector, but now we use the ciphertext as the "plaintext". Running the encryption algorithm on this will give us the "ciphertext" which is actually just the plaintext flag! We could do this by hand by copying/pasting the values, but we might as well write a quick script for it:

```python
from pwn import *

p = process('./server.py')
p = remote('ctf.ropcity.com', 31337)

p.recvuntil(b'Encrypted flag: ')

encrypted_data = bytes.fromhex(p.recvline().strip(b'\n').decode())

flag_nonce = encrypted_data[:16]
flag_data = encrypted_data[16:]

p.recv()
p.sendline(flag_data.hex())
p.recv()
p.sendline(flag_nonce.hex())
p.recvuntil('encrypted: ')
encr = bytes.fromhex(p.recvline().strip(b'\n').decode())
print(encr)
```

After running the script, we get some garbage, followed by the decrypted flag!

```
python3 solve.py
[+] Starting local process './server.py': pid 77627
[+] Opening connection to ctf.ropcity.com on port 31337: Done
b'c\r\xab\xf5\xa0\xa7\xdd\xd7\x16\xa8C\x81o:*uosu{b3TTER-Ch3cK-yOUr_n0nCe5}'
```