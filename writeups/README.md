# OSUSEC CTF League Writeups

### mash
- [thepitchdoctor](week1-thepitchdoctor.md)
- [BobbySinclusto](mash-bobbysinclusto)
- [Mike C](week1-mash-mikec.pdf)

### cowsay
- [BobbySinclusto](cowsay-bobbysinclusto)
- [thepitchdoctor](cowsay-thepitchdoctor.md)

### ret2win
- [BobbySinclusto](ret2win-bobbysinclusto)
- [thepitchdoctor](ret2win-thepitchdoctor.md)

### rsa1
- [ToxicZ](rsa1-ToxicZ)
- [thepitchdoctor](rsa1_thepitchdoctor.md)

### rsa2
- [ToxicZ](rsa2-ToxicZ)
- [thepitchdoctor](rsa2_thepitchdoctor.md)

### unprintable
- [BobbySinclusto](unprintable-bobbysinclusto)
- [thepitchdoctor](unprintable-thepitchdoctor.md)

### pwn_review
- [BobbySinclusto](pwn_review-bobbysinclusto)
- [thepitchdoctor](pwn_review-thepitchdoctor.md)

### printf_is_echo
- [BobbySinclusto](printf_is_echo-bobbysinclusto)
- [thepitchdoctor](printf_is_echo-thepitchdoctor.md)

### russian_nesting_bathhouose
- [thepitchdoctor](russian_nesting_bathhouse_thepitchdoctor/russian_nesting_bathhouse.md)
- [BobbySinclusto](russian-nesting-bathhouse-bobbysinclusto)
- [kaipan](kaipan-steg.md)

### nonsense
- [BobbySinclusto](nonsense-bobbysinclusto)

### web2
- [BobbySinclusto](web2-bobbysinclusto)
- [thepitchdoctor](web2-thepitchdoctor/web2.md)
- [kaipan](web2-kaipan.md)

### many_time_pad
- [thepitchdoctor](many_time_pad-thepitchdoctor/many_time_pad.md)
- [BobbySinclusto](many_time_pad-bobbysinclusto)
- [kaipan](many_time_pad-kaipan.md)

### cookie
- [thepitchdoctor](cookie-thepitchdoctor/cookie.md)
- [BobbySinclusto](cookie-bobbysinclusto)

### copper
- [thepitchdoctor](copper-thepitchdoctor/copper.md)

### snowcone
- [thepitchdoctor](snowcone-thepitchdoctor/snowcone.md)

### almostnopship0
- [thepitchdoctor](almostnopship0-thepitchdoctor/almostnopship0.md)

### make_a_hash
- [thepitchdoctor](make_a_hash-thepitchdoctor/make_a_hash.md)

### rayhanns_return
- [thepitchdoctor](rayhanns_return-thepitchdoctor/rayhanns_return.md)

### boxy
- [thepitchdoctor](boxy-thepitchdoctor/boxy.md)

### WEB_THREE
- [thepitchdoctor](WEB_THREE-thepitchdoctor/WEB_THREE.md)

### NCEP_XQC
- [thepitchdoctor](NCEP_XQC-thepitchdoctor/ncep_xqc.md)

### NCEP_BOTEZ
- [thepitchdoctor](NCEP_BOTEZ-thepitchdoctor/ncep_botez.md)

### scrambled_noodles
- [thepitchdoctor](scrambled_noodles-thepitchdoctor/scrambled_noodles.md)

### NCEP_MAGNUS
- [thepitchdoctor](NCEP_MAGNUS-thepitchdoctor/ncep_magnus.md)